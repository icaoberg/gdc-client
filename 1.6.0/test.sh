#!/bin/bash

singularity exec singularity-gdc-client.sif gdcclient download 22a29915-6712-4f7a-8dba-985ae9a1f005

if [ -d ./22a29915-6712-4f7a-8dba-985ae9a1f005 ]; then
	echo "Success!"
	exit 0
else
	echo "Failure..."
	exit 1
fi
